package de.telekom.example.rs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("model")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class ModelRestController {

    @PersistenceContext
    EntityManager em;

    @Path("{id}")
    @GET
    public Response getModel(@PathParam("id") String id) {
        return Response.ok(em.find(Model.class, id)).build();
    }

    @POST
    @Path("{id}")
    public Response getModel(Model model) {
        em.persist(model);
        return Response.ok(model).build();
    }

    @GET
    public Response getAll() {
        List result = em.createQuery("SELECT m FROM Model m").getResultList();
        return Response.ok(new GenericEntity<List>(result) {}).build();
    }

    @GET
    @Path("named")
    public Response getNamedAll() {
        List result = em.createNamedQuery("TEST").getResultList();
        return Response.ok(new GenericEntity<List>(result) {}).build();
    }

    @GET
    @Path("criteria")
    public Response getCriteriaAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Model> cq = cb.createQuery(Model.class);
        cq.from(Model.class);
        List<Model> result = em.createQuery(cq).getResultList();
        return Response.ok(new GenericEntity<List<Model>>(result) {}).build();
    }
}
