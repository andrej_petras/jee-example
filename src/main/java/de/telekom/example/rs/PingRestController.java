package de.telekom.example.rs;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("ping")
@Stateless
public class PingRestController {

    @Inject
    ExampleBean exampleBean;

    @GET
    public Response ping() {
        return Response.ok("PONG " + exampleBean.getText()).build();
    }
}
